"""
Created on Jan 24, 2012

Author: Steven Simmons (sds9278@g.rit.edu)
Author: Matthew Swain (mds1482@rit.edu)
"""

class Car(object):
    CAR = [None for _ in range(33)]    # A list of all the cars

    __slots__ = ('row', 'col', 'route', 'playerId')

    def __init__(self, row, col, route, playerId):
        self.row = row
        self.col = col
        self.route = route
        self.playerId = playerId


def getCarStartLocation(carId):
    """
    getCarStartLocation: (Int, Int, Int)
        
    Returns the starting row, column, and route for the given Car
    """

    car = Car.CAR[carId]
    return (car.row, car.col, car.route)

def getCarEndLocation(carId):
    """
    getCarStartLocation: (Int, Int, Int)
    
    Returns the ending row, column, and route for the given Car
    """

    car = Car.CAR[carId]
    
    route = car.route - 1
    
    return car.row, car.col, car.route - 1

def getNeighboringCars(row, col):
    """
    getNeighboringCars: (Int, Int) -> [Int]
    
    Returns a list of all car numbers that surround (row, col)
    If there are none, an empty list is returned
    """

    # Bounds checking (make sure coordinates are inside the board)
    if row < 0 or row > 7 or col < 0 or col > 7:
        return []

    # If row, col not on an edge, no neighboring cars
    elif row != 0 and row != 7 and col != 0 and col != 7:
        return []

    result = []

    if row == 0:
        result.append(col + 1)

    if col == 0:
        result.append(32 - row)

    if row == 7:
        result.append(24 - col)

    if col == 7:
        result.append(row + 9)

    return result

def getCarOwner(carId):
    """
    getCarOwner: Int -> Int
    Given a carId from 1-32, returns the playerId that the car belongs to (0-5)
    """

    assert (carId >= 1 and carId <= 32)
    if carId < 1 or carId > 32:
        return None
    
    return Car.CAR[carId].playerId

def setup(numPlayers):
    """
    setup: ()

    Sets up all Cars so that they contain their starting row, column, and route number
    Should only be called once, but is safe to call multiple times
    """

    carOwner = [[] for i in range(6)]
    # Car Owner [Num players - 1][Car id] = owner player id
    # One player game
    carOwner[0] = [0 for _ in range(32)]
    # Two player game
    carOwner[1] = [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
    # Three player
    carOwner[2] = [0, 1, 2, 0, 2, 0, 1, 2, 1, 2, 0, 1, 2, 1, 0, -1, -1, 2, 1, 0, 2, 1, 0, 2, 0, 2, 1, 0, 1, 2, 0, 1]
    # Four player
    carOwner[3] = [2, 3, 1, 0, 3, 2, 0, 1, 3, 2, 0, 1, 2, 3, 1, 0, 3, 2, 1, 0, 2, 3, 0, 1, 2, 3, 0, 1, 3, 2, 1, 0]
    # Five player
    carOwner[4] = [0, 3, 2, 4, 0, 1, 2, 4, 3, 0, 4, 1, 3, 0, 2, -1, -1, 1, 2, 4, 3, 0, 1, 4, 2, 3, 1, 0, 2, 3, 4, 1]
    # Six Player
    carOwner[5] = [0, 1, 4, 2, 0, 3, 5, 2, 4, 0, 1, 5, 4, 2, 3, -1, -1, 1, 0, 3, 2, 5, 4, 3, 1, 2, 0, 5, 1, 4, 3, 5]

    # Create a Car object for each station
    # Has starting row, column, starting route # (0-7), 
    for i in range(1, 9):

        Car.CAR[i]    = Car(0, i - 1, 0, carOwner[numPlayers-1][i-1])
        Car.CAR[i+8]  = Car(i - 1, 7, 2, carOwner[numPlayers-1][i+7])
        Car.CAR[25-i] = Car(7, i - 1, 4, carOwner[numPlayers-1][24-i])
        Car.CAR[33-i] = Car(i - 1, 0, 6, carOwner[numPlayers-1][32-i])
