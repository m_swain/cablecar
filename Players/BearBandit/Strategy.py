"""
Assumes that this will be a 2 player game and won't be effective against more opponents
"""

from Misc import *
from copy import deepcopy

def scoreMove(oldData, newData, row, col, rotation, playerId, opponentId, analyzeOpponent=True):
    """
    scoreMove: PlayerData * PlayerData * Int * Int -> Int

    Parameters:
        oldData         - PlayerData structure that contains board before current tile placed
        newData         - PlayerData structure that contains board after current tile placed
        row             - Row of placed tile
        column          - Column of placed tile
        rotation        - Rotation of placed tile
        playerId        - playerId of player to be scored
        opponentId      - playerId of opponent to be scored against
        analyzeOpponent -
        
    Result: Int
        An integer score for the given move. Higher scores should reflect better moves
    """

    oldScores, oldCompletes = calculateScores(oldData)
    newScores, newCompletes = calculateScores(newData)

    # Calculate differences in score for each player before and after the move
    opponentScoreDifference = 0
    if newData.numPlayers > 1:
        opponentScoreDifference = newScores[opponentId] - oldScores[opponentId]
    playerScoreDifference = newScores[playerId] - oldScores[playerId]
    scoreDifference = playerScoreDifference - opponentScoreDifference

    # Calculate number of complete routes for each player before and after
    opponentCompleteDifference = 0
    if newData.numPlayers > 1:
        opponentCompleteDifference = newCompletes[opponentId] - oldCompletes[opponentId]
    playerCompleteDifference = newCompletes[playerId] - oldCompletes[playerId]
    completeDifference = opponentCompleteDifference * 2 - playerCompleteDifference * 3
    
    difference = scoreDifference + completeDifference * 10
    
    analyzeScore = 0
    if newData.numPlayers > 1 and analyzeOpponent and newData.opponentTiles[opponentId] != None:
        # Compute the score of what we believe will be the best move for the opponent
        # Note: at the start of the game, we don't have the opponent's tile info!

        dataCopy = deepcopy(newData)

        dataCopy.playerId = opponentId
        dataCopy.currentTile = dataCopy.opponentTiles[opponentId]
    
        dataCopy.board[row][col] = Tile(newData.currentTile, rotation)
        updateValidPositions(dataCopy, row, col)
    
        analyzeScore = selectMove(dataCopy, False)[1]

    # Score calculation, value # of complete routes twice as much
    return difference - analyzeScore

def selectMove(playerData, analyzeOpponent=True):
    """
    selectMove: PlayerData -> (Int, Int, Int)

    From a given set of moves, returns the move that should be played

    Arguments:
        playerData - PlayerData structure for BearBandit

    Result: ((Int, Int, Int), Int)
        (Int, Int, Int) - A tuple of the row, column, and rotation of the selected move
        Int             - The score of the selected move
    """

    validMoves = getValidMoves(playerData)
    assert( len(validMoves) > 0 )
    
    tileType = playerData.currentTile
    bestMove = None
    bestScore = -99999

    playerDataCopy = deepcopy(playerData)
    playerId = playerData.playerId
    opponentId = 0
    
    if playerId == 0:
        opponentId = 1
    
    for move in validMoves:
        row, col, rotation = move
        
        # Make move on copied board, score it, then erase move
        playerDataCopy.board[row][col] = Tile(tileType, rotation)
        score = scoreMove(playerData, playerDataCopy, row, col, rotation, playerId, opponentId, analyzeOpponent)
        playerDataCopy.board[row][col] = None
        
        if score > bestScore:
            bestMove = move
            bestScore = score

    
    return bestMove, bestScore
        