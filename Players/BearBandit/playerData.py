"""
Cable Car: Student Computer Player

A sample class you may use to hold your state data
Author: Adam Oest (amo9149@rit.edu)
Author: Steven Simmons (sds9278@g.rit.edu)
Author: Matthew Swain (mds1482@rit.edu)
"""

from Tile import Tile

class PlayerData(object):
    ROWS = 8
    COLS = 8

    # Add other slots as needed
    __slots__ = ('logger', 'playerId', 'currentTile', 'numPlayers', 'board', 'validPositions', 'opponentTiles')

    def __init__(self, logger, playerId, currentTile, numPlayers):
        """
        __init__: PlayerData * Engine.Logger * int * NoneType * int -> None
        Constructs and returns an instance of PlayerData.
            self - new instance
            logger - the engine logger
            playerId - my player ID (0-5)
            currentTile - my current hand tile (string[a-j], initially None)
            numPlayers - number of players in game (1-6)
        """

        self.logger = logger
        self.playerId = playerId
        self.currentTile = currentTile
        self.numPlayers = numPlayers

        # Contains the board and all tiles
        self.board = [[None for _ in range(8)] for _ in range(8)]
        
        # Contains the tiles for each opponent
        self.opponentTiles = [None for _ in range(numPlayers)]
        
        # Contains a list of (row, col) that are empty and can potentially have tiles placed on them
        # Initially, this contains only edge locations
        self.validPositions = []
        
        # Add the locations along the top and bottom of the board to the list
        for i in range(8):
            # Add locations along the top/bottom of the board (including corners) to the list
            self.validPositions.append( (0, i) )
            self.validPositions.append( (7, i) )
            
            # Add positions from the extreme left/right of the board (excluding corners)
            if i == 0 or i == 7:
                continue
            
            self.validPositions.append( (i, 0) )
            self.validPositions.append( (i, 7) )
        
    def getTile(self, row, col):
        """
        getTile: Int * Int -> (Tile or None)
        
        Returns the tile at (row, col) or None if there is no tile there
        """

        # Is a power station at the given row/col?
        if (row == 3 or row == 4) and (col == 3 or col == 4):
            return Tile('ps', 0)

        # If the row or col is outside of the board, it's considered a station
        if row < 0 or row >= PlayerData.ROWS or col < 0 or col >= PlayerData.COLS:
            return Tile('station', 0)

        return self.board[row][col]

    def __str__(self):
        """
        __str__: PlayerData -> string
        Returns a string representation of the PlayerData object.
            self - the PlayerData object
        """

        result = "PlayerData= " \
                    + "playerId: " + str(self.playerId) \
                    + ", currentTile: " + str(self.currentTile) \
                    + ", numPlayers:" + str(self.numPlayers)

        # add any more string concatenation for your other slots here

        return result