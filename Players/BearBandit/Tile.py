"""
Created on Jan 24, 2012

Author: Steven Simmons (sds9278@g.rit.edu)
Author: Matthew Swain (mds1482@rit.edu)
"""

class Tile(object):
    TILE_ROUTES = {}

    DIRECTION_UP = 0
    DIRECTION_RIGHT = 1
    DIRECTION_DOWN = 2
    DIRECTION_LEFT = 3

    __slots__ = ('type', 'rotation')
    #Rotates clockwise

    def __init__(self, type, rotation):
        self.type = type
        self.rotation = rotation
        
    def isTile(self):
        """
        isTile: Bool
        
        Returns whether or not this Tile object refers to a tile on the board
        (As opposed to being a power station or car station)
        """

        if self.type == 'ps' or self.type == 'station':
            return False

        return True

    def getRoute(self, start):
        """
        get_route: Int -> Int, Direction
        
        Given a starting point (0-7) which describes a specific path on the tile,
        this gives an exit direction (Up, Down, Left, Right) and an entrance point for the next tile
        
        Ex:    type = a, rotation = 1, start = 3
        
        After rotating, the starting position of 3 ends up at the exit point 2
        An exit point of 2 means that the route is heading RIGHT and will be entering the tile to its right
        at position 7
        """

        routes = Tile.TILE_ROUTES[self.type]
        
        # Rotation is clockwise, so the starting position is rotated counter-clockwise (because the tile isn't rotated)
        start -= self.rotation * 2
        if start < 0:
            start += 8

        # Get the exit route from our new start point
        exit = routes[start]
        
        # Rotate the end-point clockwise to get the correct exit port number
        exit += self.rotation * 2
        if exit > 7:
            exit -= 8
        
        # Based on the exit port number, determine the exit direction
        direction = Tile.DIRECTION_LEFT
        if exit < 2:
            direction = Tile.DIRECTION_UP
        elif exit < 4:
            direction = Tile.DIRECTION_RIGHT
        elif exit < 6:
            direction = Tile.DIRECTION_DOWN
        
        # exit is the number of the outgoing port (0-7)
        # This needs to be changed to what will be the incoming port for the next tile
        # Ex: Out: 7 -> In: 2, Out: 0 -> In: 5

        result = 0
        if exit % 2 == 0:
            result = exit - 3
        else:
            result = exit - 5

        if result < 0:
            result += 8

        return result, direction

def setup():
    """
    setup: ()
    Sets the routes for each tile type a-j in the TILE_ROUTES dictionary
    This method should only be called once, although calling it multiple times is harmless
    """
        
    Tile.TILE_ROUTES['a'] = [1, 0, 7, 6, 5, 4, 3, 2]
    Tile.TILE_ROUTES['b'] = [3, 4, 7, 0, 1, 6, 5, 2]
    Tile.TILE_ROUTES['c'] = [3, 4, 5, 0, 1, 2, 7, 6]
    Tile.TILE_ROUTES['d'] = [1, 0, 7, 4, 3, 6, 5, 2]
    Tile.TILE_ROUTES['e'] = [1, 0, 3, 2, 7, 6, 5, 4]
    Tile.TILE_ROUTES['f'] = [5, 4, 7, 6, 1, 0, 3, 2]
    Tile.TILE_ROUTES['g'] = [1, 0, 3, 2, 5, 4, 7, 6]
    Tile.TILE_ROUTES['h'] = [7, 2, 1, 4, 3, 6, 5, 0]
    Tile.TILE_ROUTES['i'] = [3, 6, 5, 0, 7, 2, 1, 4]
    Tile.TILE_ROUTES['j'] = [7, 6, 5, 4, 3, 2, 1, 0]
        
setup()