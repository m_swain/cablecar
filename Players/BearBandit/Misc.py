from playerData import PlayerData
from Tile import Tile
from Car import getCarStartLocation, getNeighboringCars, getCarOwner


"""
Move calculation functions
"""

def updateValidPositions(playerData, row, col):
    """
    updateValidPositions: PlayerData * Int * Int -> ()
    
    Updates playerData.validPositions so that it:
      1. No longer contains (row, col)
      2. Contains all valid tile locations adjacent to (row, col)
    """
    
    playerData.validPositions.remove((row, col))

    up =    (row-1, col)
    down =  (row+1, col)
    left =  (row, col-1)
    right = (row, col+1)

    for position in [up, down, left, right]:
        row, col = position
        
        if (not playerData.getTile(row, col)) and (not position in playerData.validPositions):
            playerData.validPositions.append(position)

def checkTilePlacement(playerData, row, col, tileType):
    """
    checkTilePlacement: PlayerData * Int * Int * Char -> (Bool, Int) or Bool
    
    Tries to place the given tileType at (row, col) using all rotations
    Result:
        (True, tile rotation) = Was able to place tile
        False                 = Unable to place tile
    """

    # Try all rotations (0-3)
    for rotation in range(4):
            playerData.board[row][col] = Tile(tileType, rotation)
            cars = getNeighboringCars(row, col)
            
            # No stations nearby? Done!
            if not cars:
                playerData.board[row][col] = None
                return True, rotation

            # Check nearby cars to make sure none have a complete route with score = 1
            success = True
            for car in cars:
                complete, score = getRouteInfo(playerData, car)
                
                if complete and score == 1:
                    success = False
                    break

            if success:
                playerData.board[row][col] = None
                return True, rotation

    # Unsuccessful           
    playerData.board[row][col] = None
    return False
            
def getValidMoves(playerData):
    """
    getValidMoves: PlayerData -> [(Int, Int, Int)]
    
    Returns a list of all valid tile placements (row, col, rotation) for the current tile
    """

    assert (len(playerData.validPositions) > 0)
    
    validMoves = []
    tileType = playerData.currentTile

    # Try to make a move in all valid positions
    success = False
    for position in playerData.validPositions:
        row, col = position

        assert (playerData.board[row][col] == None)
        
        # Check all rotations to see if a tile can be placed
        result = checkTilePlacement(playerData, row, col, tileType)
        if result:
            validMoves.append( (row, col, result[1]) )
            success = True
    
    # If no tiles able to be placed without offending the route of length 1 rule
    # All validPositions are also valid moves
    
    if not success:
        for position in playerData.validPositions:
            row, col = position
        
            for rotation in range(4):
                validMoves.append( (row, col, rotation) )

    return validMoves

"""
Score calculation functions
"""

def getRouteInfo(playerData, carId):
    """
    getRouteInfo: PlayerData * Int -> (Bool, Int)
    Returns info on the route starting at carId
    
    Parameters:
        playerData: PlayerData
        The player data structure
        
        carId: Int
        The station number where the route starts
    
    Result: (Bool, Int)
        Bool - Whether or not the route starting at carId is complete
        Int  - The score of the route starting at carId
    """

    row, col, route = getCarStartLocation(carId)

    tile = playerData.getTile(row, col)
    score = 0

    while tile:
        
        # Ended at a power station
        if tile.type == 'ps':
            return True, score * 2
        
        elif tile.type == 'station':
            return True, score

        score += 1
        route, direction = tile.getRoute(route)

        if direction == Tile.DIRECTION_UP:
            row -= 1
        elif direction == Tile.DIRECTION_LEFT:
            col -= 1
        elif direction == Tile.DIRECTION_DOWN:
            row += 1
        elif direction == Tile.DIRECTION_RIGHT:
            col += 1

        tile = playerData.getTile(row, col)

    # Route is incomplete
    return False, score

def calculateScores(playerData):
    """
    calculateScores: PlayerData -> ([Int], [Int])
    
    Returns a list of the scores and number of complete routes for each player
    
    Result: ([Int], [Int])
        [Int] - A list of scores. Index corresponds to playerId
        [Int] - A list of complete routes. Index corresponds to playerId
    """
    
    # Calculate the scores for each player
    scores = [0 for i in range(playerData.numPlayers)]
    completes = [0 for i in range(playerData.numPlayers)]

    # Get the score for each car
    for i in range(1, 33):
        complete, score = getRouteInfo(playerData, i)
        
        owner = getCarOwner(i)
        
        # if owner == -1, station has no owner
        if owner >= 0:
            
            scores[owner] += score
            
            if complete:
                completes[owner] += 1
    
    return scores, completes