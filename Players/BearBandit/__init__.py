from Model.interface import PlayerMove
from playerData import PlayerData
from Tile import Tile
from Car import getCarStartLocation, getNeighboringCars, getCarOwner
from Misc import getRouteInfo, calculateScores, updateValidPositions
from Strategy import selectMove

"""
Author: Adam Oest (amo9149@rit.edu)
Author: Steven Simmons (sds9278@g.rit.edu)
Author: Matthew Swain (mds1482@rit.edu)
"""

def makeMove(playerData):
    """
    makeMove: PlayerData -> (Int, Int, Char, Int)
    
    Makes a move on the board and returns the chosen row, col, tileType, and rotation
    Will update the board with the placed tile as well as the list of
    valid locations to place future tiles
    """

    tileType = playerData.currentTile
    row, col, rotation = selectMove(playerData)[0]
    
    # Place tile on board and update valid locations
    playerData.board[row][col] = Tile(tileType, rotation)
    updateValidPositions(playerData, row, col)
    
    return row, col, tileType, rotation

def init(playerId, numPlayers, startTile, logger, arg = "None"):
    """The engine calls this function at the start of the game in order to:
        -tell you your player id (0 through 5)
        -tell you how many players there are (1 through 6)
        -tell you what your start tile is (a letter a through i)
        -give you an instance of the logger (use logger.write("str") 
            to log a message) (use of this is optional)
        -inform you of an additional argument passed 
            via the config file (use of this is optional)

    Parameters:
        playerId - your player id (0-5)
        numPlayers - the number of players in the game (1-6)
        startTile - the letter of your start tile (a-j)
        logger - and instance of the logger object
        arg - an extra argument as specified via the config file (optional)

    You return:
        playerData - your player data, which is any data structure
                     that contains whatever you need to keep track of.
                     Consider this your permanent state.
    """
    Car.setup(numPlayers)
    
    # Put your data in here.  
    # This will be permanently accessible by you in all functions.
    # It can be an object, list, or dictionary
    playerData = PlayerData(logger, playerId, startTile, numPlayers)

    # This is how you write data to the log file
    playerData.logger.write("Player %s starting up" % playerId)
    
    # This is how you print out your data to standard output (not logged)
    print(playerData)
    
    return playerData

def move(playerData):  
    """The engine calls this function when it wants you to make a move.
    
    Parameters:
        playerData - your player data, 
            which contains whatever you need to keep track of
        
    You return:
        playerData - your player data, 
            which contains whatever you need to keep track of
        playerMove - your next move
    """
    location = None
    tile = None
    
    playerData.logger.write("move() called")
    
    row, col, tile, tileRotation = makeMove(playerData)

    # Populate these values
    playerId = playerData.playerId
    position = (row, col)
    tileName = tile
    rotation = tileRotation

    return playerData, PlayerMove(playerId, position, tileName, rotation)

def move_info(playerData, playerMove, nextTile):
    """The engine calls this function to notify you of:
        -other players' moves
        -your and other players' next tiles
        
    The function is called with your player's data, as well as the valid move of
    the other player.  Your updated player data should be returned.
    
    Parameters:
        playerData - your player data, 
            which contains whatever you need to keep track of
        playerMove - the move of another player in the game, or None if own move
        nextTile - the next tile for the player specified in playerMove, 
                    or if playerMove is None, then own next tile
                    nextTile can be none if we're on the last move
    You return:
        playerData - your player data, 
            which contains whatever you need to keep track of
    """

    playerData.logger.write("move_info() called")

    # Our move, board already updated
    if not playerMove:

        if nextTile:
            playerData.currentTile = nextTile

        return playerData
    
    # Opponent's move, update their currentTile
    playerData.opponentTiles[playerMove.playerId] = nextTile

    # Update the board to include the moves made by other player
    board = playerData.board
    row, column = playerMove.position

    tile = Tile(playerMove.tileName, playerMove.rotation)
    board[row][column] = tile
    updateValidPositions(playerData, row, column)

    return playerData


def game_over(playerData, historyFileName = None):
    """The engine calls this function after the game is over 
        (regardless of whether or not you have been kicked out)

    You can use it for testing purposes or anything else you might need to do...
    
    Parameters:
        playerData - your player data as always       
        historyFileName - name of the current history file, 
            or None if not being used 
    """
    
    # Calculate the scores for each player
    scores = calculateScores(playerData)[0]
    
    winningScore = scores[0]
    winners = []
    
    # Print score for each player and select winner(s)
    for i in range(playerData.numPlayers):
        print("Player " + str(i) + ": score = " + str(scores[i]))
        
        if scores[i] > winningScore:
            winningScore = scores[i]
            winners = [i]
        elif scores[i] == winningScore:
            winners.append(i)
            
    # Print the winner(s)
    if len(winners) == 1:
        print("Player #" + str(winners[0]) + " has won")
    else:
        print "Players #" + str(winners[0]),

        for i in range(1, len(winners)):
            print ",#" + str(winners[i]),
            
        print(" have won.")
